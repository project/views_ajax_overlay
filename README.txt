INTRODUCTION
------------
Views Ajax Overlay is a simple View plugin that adds a loading overlay to the 
view on ajax callbacks (pager, filter etc...).

REQUIREMENTS
------------
This module requires the following modules:
 * Views (https://drupal.org/project/views)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * To enable/disable the ajax overlay on ALL ajax-enabled views by default, 
   go to Structure >> Views >> Settings >> Ajax Overlay

 * To enable/disable the overlay per view, navigate to the 'Enable Ajax' link in
   the 'Other' section of the view configuration screen.

CUSTOMIZATION
-------------
To change the loading 'throbber' image, alter the following CSS:

div.view-ajax-overlay div.overlay {
 background: url(./path) center no-repeat rgba(255,255,255,0.7) !important;
}
