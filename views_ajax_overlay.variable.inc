<?php

/**
 * @file
 * Hooks and functions used by the Variable module.
 */

/**
 * Implements hook_variable_info().
 */
function views_ajax_overlay_variable_info($options) {
  $variables['views_ajax_overlay_default'] = array(
    'title' => t('Enable overlay on all ajax-enabled views by default.'),
    'description' => t('When enabled, all views with ajax will show the overlay.
    To control each view on a more granular level, enable/disable the loading overlay from the view options.'),
    'type' => 'boolean',
    'default' => FALSE,
  );

  // Views Ajax Overlay message.
  $variables['views_ajax_overlay_message'] = array(
    'title' => t('Overlay message', array(), $options),
    'type' => 'string',
    'required' => TRUE,
    'localize' => TRUE,
    'default' => t('Loading...', array(), $options),
  );

  return $variables;
}
