<?php

/**
 * @file
 * Admin settings for the Views Ajax Overlay module.
 */

/**
 * Configuration screen to enable/disable ajax overlay globally.
 *
 * @return mixed
 *   The form structure.
 */
function views_ajax_overlay_settings() {
  $form = array();

  $form['views_ajax_overlay_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable overlay on all ajax-enabled views by default.'),
    '#description' => t('When enabled, all views with ajax will show the overlay.
    To control each view on a more granular level, enable/disable the loading overlay from the view options.'),
    '#default_value' => variable_get('views_ajax_overlay_default', FALSE),
  );

  return system_settings_form($form);
}
