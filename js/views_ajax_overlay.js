/**
 * @file
 * Enables the ajax loading overlay before the actual Ajax call.
 */

(function ($) {
  var beforeSend = Drupal.ajax.prototype.beforeSend,
      success = Drupal.ajax.prototype.success,
      error = Drupal.ajax.prototype.error;

  Drupal.ajax.prototype.beforeSend = function(xmlhttprequest, options) {
    // Add loading overlay on all ajax-enabled views.
    var isChildView = $(this.selector).parents('.view-ajax-overlay').length;

    if ($(this.selector).hasClass('view-ajax-overlay') && !isChildView) {
      Drupal.behaviors.viewsAjaxOverlay.loadAjaxOverlay($(this.selector));
    }

    beforeSend.call(this, xmlhttprequest, options);
  };

  Drupal.ajax.prototype.success = function(response, status) {
    // Fail-safe to remove loading overlay on all ajax-enabled views.
    Drupal.behaviors.viewsAjaxOverlay.removeAjaxOverlay($(this.selector));

    success.call(this, response, status);
  };

  Drupal.ajax.prototype.error = function(response, status) {
    // Fail-safe to remove loading overlay on all ajax-enabled views.
    Drupal.behaviors.viewsAjaxOverlay.removeAjaxOverlay($(this.selctor));

    error.call(this, response, status);
  };
  
  Drupal.behaviors.viewsAjaxOverlay = {
    $overlay: $('<div class="loading-overlay">' +
                '<div class="loader">' +
                '<div class="loader__animation"></div>' +
                '<div class="loader__message"></div></div></div>'),

    attach: function (context, settings) {
      if (settings.views_ajax_overlay && settings.views_ajax_overlay.message) {
        this.$overlay.find('.loader__message').text(settings.views_ajax_overlay.message);
      }
    },

    /**
     * Appends the overlay to a given view.
     *
     * @param $view
     */
    loadAjaxOverlay: function ($view) {
      var offsetY,
          elHeight,
          $loader = this.$overlay.children('.loader'),
          $content = $view.children('.view-content'),
          $empty = $view.children('.view-empty:first');

      // Reset inline styles.
      $loader.removeAttr('style');

      if ($content.length) {
        offsetY = this.getElementViewPortCenter($content);

        $loader.css('top', offsetY);
        $content.prepend(this.$overlay);
      }
      else if ($empty.length) {
        elHeight = $empty.outerHeight();

        // For very small containers limit the throbber to the container's height.
        if (elHeight < 50) {
          $loader.css('background-size', 'auto ' + elHeight + 'px');
        }

        $empty.append(this.$overlay);
      }
    },

    /**
     * Removes the overlay from a given view.
     *
     * @param $view
     */
    removeAjaxOverlay: function ($view) {
      if ($view.hasClass('view-ajax-overlay')) {
        $view.find('.loading-overlay').remove();
      }
    },

    /**
     * Helper function to get the center of an element within the viewport.
     *
     * @param $element
     * @returns {*}
     */
    getElementViewPortCenter: function ($element) {
      var scrollTop = $(window).scrollTop(),
          scrollBot = scrollTop + $(window).height(),
          elHeight = $element.outerHeight(),
          elTop = $element.offset().top,
          elBottom = elTop + elHeight,
          elTopOffset = elTop < scrollTop ? scrollTop - elTop : 0,
          elBottomOffset = elBottom > scrollBot ? scrollBot - elTop : elHeight;

      // Return 50% if entire element is visible.
      if (elTopOffset === 0 && elBottomOffset === elHeight) {
        return '50%';
      }

      return Math.round(elTopOffset + ((elBottomOffset - elTopOffset) / 2)) + 'px';
    }

  };

})(jQuery);
