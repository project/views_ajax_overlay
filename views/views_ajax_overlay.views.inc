<?php

/**
 * @file
 * Add new views dislay plugin.
 */

/**
 * Implements hook_views_plugin().
 */
function views_ajax_overlay_views_plugins() {
  $plugins = array();
  $plugins['display_extender']['views_ajax_overlay'] = array(
    'title' => t('Add Ajax Overlay'),
    'uses options' => TRUE,
    'handler' => 'views_ajax_overlay_plugin_display_ajax_overlay',
  );

  return $plugins;
}
