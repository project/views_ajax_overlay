<?php

/**
 * @file
 * Contains the new ajax overlay views plugin.
 */

/**
 * Class views_ajax_overlay_plugin_display_ajax_overlay.
 */
class views_ajax_overlay_plugin_display_ajax_overlay extends views_plugin_display_extender {

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_definition_alter(&$options) {
    $options['use_ajax_overlay'] = array(
      'default' => variable_get('views_ajax_overlay_default', FALSE),
      'bool' => TRUE,
    );

    return $options;
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    if ($form_state['section'] == 'use_ajax') {
      // Merge this form with the 'Use ajax' form.
      $form['use_ajax_overlay'] = array(
        '#title' => t('Apply overlay effect on view:'),
        '#description' => t('Applies a loading overlay on top of the view for each ajax callback.'),
        '#type' => 'radios',
        '#options' => array(1 => t('Yes'), 0 => t('No')),
        '#default_value' => $this->display->get_option('use_ajax_overlay') ? 1 : 0,
        '#states' => array(
          'visible' => array(
            ':input[name="use_ajax"]' => array('value' => 1),
          ),
        ),
      );
    }
  }

  /**
   * Handle any special handling on the validate form.
   */
  function options_submit(&$form, &$form_state) {
    if ($form_state['section'] == 'use_ajax') {
      $use_ajax_overlay = $form_state['values']['use_ajax_overlay'];

      $this->display->set_option('use_ajax_overlay', (bool) $use_ajax_overlay);
    }
  }

  /**
   * Provide the default summary for options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    $use_ajax = $this->display->get_option('use_ajax');
    $use_ajax_overlay = $this->display->get_option('use_ajax_overlay');

    if ($use_ajax && $use_ajax_overlay) {
      $options['use_ajax']['value'] = t('Use AJAX, with loading overlay');
    }
  }

}
